import React from 'react';
import App from './App';
import { render } from "react-dom"
import * as serviceWorker from './serviceWorker';


render(<App/>, document.getElementById('root'));

serviceWorker.unregister();
