import React, { Component, lazy, Suspense } from 'react';
import {BrowserRouter as Router,Route,Redirect,Switch} from "react-router-dom";
import Auth from './pages/authPage';
import Main from './pages/Main';
import widthRouter from './hoc/widthRouter';
import ServerUserComponent from './hoc/ServerUserComponent';
import widthLoading from './hoc/widthLoading';

class App extends Component {

async componentDidMount () {
  await Promise.resolve(this.props).then(props=>{
    const {auth,history} = props;
    auth ? history.push('/main') : history.push('/auth');
  })
  
}
  render() {
    const {functions,auth,...rest} = this.props;

    
      return (
        <div>
              <Switch>
               
                <Route path={'/auth'} render = {
                    (props)=> !auth ? <Auth {...props} auth={auth} signIn={functions.auth}/> : <Redirect to={'/main'} /> }/> 
                <Route path={'/main'} render = {(props)=>  auth ? <Main {...props} functions={functions}/> : <Redirect to={'/auth'} />} />
              </Switch>
        </div>
      );
    }
    
}


export default  widthLoading(ServerUserComponent(widthRouter(App)));

