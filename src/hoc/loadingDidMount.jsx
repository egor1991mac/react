import React from 'react';


export default function loadingDidMount(Component){

    return class Loading extends Component {
            constructor(props){
                super(props);
            }
            
            state = {
                loading: false,
                component: null
            };
            
            async componentDidMount(){
                if(super.componentDidMount()){
                    
                }
            }

            render(){
                return this.state.loading ? this.state.component != null ? this.state.component : <div>...loading...</div> : super.render();
            }

    }

}

