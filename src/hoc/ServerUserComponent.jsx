import React from 'react';
import firebase from 'firebase';
import {database,user} from '../config';



               
export default function serverComponent (Component){
    

    return class ServerComponent extends React.Component {
        constructor(props){
            super(props);
            this.uid = null;
            
        }
            state = {
                auth:false,
                user: 'guest',
                role:'guest'
            }
           
       async componentDidMount() {
           
                
            if(!sessionStorage.getItem('user')){
                 await user.onAuthStateChanged((user)=>{
                        if (user) {
                            this.setState({auth:true});
                            this.uid = user.uid; 
                            this.getUserRole(user.uid);
                          
                        } else {
                            this.setState({auth:false});
                        }
                      });
                }
                else{
                    this.uid = JSON.parse(sessionStorage.getItem('user')).user;
                    this.setState({auth:true, role:JSON.parse(sessionStorage.getItem('user')).role });
                    
                    
                } 
            }
           
        auth = async (email,password) =>{
                
                    if(!this.state.auth){
                        this.setState({loading:true});
                        try{
                        let auth = await user.signInWithEmailAndPassword(email, password);
                        this.getUserRole(auth.user.uid);
                        this.setState({loading:false,auth:true});
                        alert('Вы вошли в систему');
                        }
                        catch(e){
                            alert('Не верный логин или пароль');
                            this.setState({loading:false,auth:false});
                        }
                        
                        
                    }
            }
            signOut = async () =>{
                
                if(this.state.auth){
                    this.setState({loading:true});
                    await user.signOut();
                    this.setState({loading:false,auth:false});
                    sessionStorage.clear();
                    alert('Вы вышли из системы');
                }
               
            }
            getUserRole = (data) =>{
                
               const table = database.child('users_role');
               table.on('value', snap => {
                   const arrUsersRole = snap.val();
                    for(let key in arrUsersRole){
                      arrUsersRole[key].forEach(element => {
                            if(element == data){
                                sessionStorage.setItem('user', JSON.stringify({user:data,role:key}));
                                this.setState({role:key});
                            }
                        });
                    
                    }
               })
            }
            currentUserInfo = async () => {
                let data;
                let table = database.child('users_list').orderByChild('uid').equalTo(this.uid);
                table.on('value', async snap=>{
                    data = snap.val();
                    this.setState({user:snap.val()});
                })
               
                return await data;
            }            
            render(){
          
                return (<Component {...this.props} {...this.state} 
                                        functions = {
                                                        {  
                                                            auth:this.auth,
                                                            signOut:this.signOut,
                                                            currentUserInfo:this.currentUserInfo
                                                        }
                                                    } 
                />)
            }

    }

}

