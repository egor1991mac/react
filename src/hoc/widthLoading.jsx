import React from 'react';
import Loader from '../components/loading';

export default function widthLoading(Component){

    class Loading extends Component {
            constructor(props){
                super(props);
            }
            
            state = {
                loading: true
               
            };
           async componentDidMount(){
                if(super.componentDidMount){
                    await super.componentDidMount();
                   
                }
                this.setState({loading:false});
                
                
            }

            render(){
               
                return this.state.loading ? <Loader/> : super.render();
            }

    }
    Loading.displayName = `widthLoading(${Component.displayName || Component.name || 'Component'})`;
    
    return Loading;
}

