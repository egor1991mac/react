import React from 'react';
import {BrowserRouter as Router,Route,Redirect,Switch} from "react-router-dom";

export default function serverComponent (Component){

    return class widthRouter extends React.Component {
          
          constructor(props){
              super(props);
          }
        
         
            render(){
                const {...rest} = this.props;
                return (
                    <Router>
                        <Route path="/" render = {
                            (props)=> <Component {...props} {...rest} />
                        }/>
                    </Router>
                )
                
            }

    }

}


