import React from 'react';
import Header from './Header';
import Content from './Content';
import withData from '../../hoc/ServerDataComponent';
import UserInfoPage from '../userInfo';
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
class Main extends React.Component {
  
  async componentDidMount(){
    await Promise.resolve()
  }
  render() {
    const {match,functions,...rest} = this.props;
    return (

      <div>
        <Header match = {match} functions={functions.signOut} />
          <Switch>
            <Route path={`${match.path}/userinfo`} render={(props)=><UserInfoPage {...this.props}/> } />
            <Route path="/" render={(props)=><Content />}/>
          </Switch>
        </div>
    );
  }
}



export default withData(Main);