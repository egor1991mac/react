import React, { Component } from 'react';
import Card from '../../../components/Card'
import Tooltip from '../../../components/Tooltip'

const style = {
    root:{
        display:'flex',
        flexWrap: 'wrap',
        justifyContent:'center',
        margin:'2rem 0'
    }
}


export default class Content extends Component {
  render() {

    return (
      <div style={style.root}>
         <Card/>
         <Tooltip/>
      </div>
    )
  }
}
