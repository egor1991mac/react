import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {Link} from "react-router-dom";
import Aside from '../Aside';
import LeftMenu from '../LeftMenu';
//import dravs from '../Dravs';
const styles = {
  root: {
    justifyContent:'space-between',
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  
};

class Header extends React.Component {
  state = {
    auth: true,
    anchorEl: null,
    openAside: false
  };

  handleChange = event => {
    this.setState({ auth: event.target.checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  HandleOpenAside = () =>{
      this.setState({openAside: !this.state.openAside});

  }
  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes,match,functions } = this.props;
    const { auth, anchorEl, openAside } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div className={classes.root}>
         
        <AppBar position="static">
          <Toolbar className={classes.root}>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.HandleOpenAside}>
              <MenuIcon />
            </IconButton>
            {auth && (
              <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem component={Link} to={`${match.path}/userinfo`}> Мой аккаунт </MenuItem>
                 <button onClick ={functions}>Выйти</button>
                </Menu>
              </div>
            )}
          </Toolbar>
        </AppBar>
        <Aside  open = {openAside} handleOpen = {this.HandleOpenAside}>
            <LeftMenu/>
        </Aside>
      </div>
    );
  }
}



export default withStyles(styles)(Header);