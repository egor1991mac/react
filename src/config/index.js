import firebase from 'firebase';

export const configFirebase = {
    apiKey: "AIzaSyCCvLORQrLtWugXpuO9GwRP3ma1GlKVouI",
    authDomain: "mini-sclad.firebaseapp.com",
    databaseURL: "https://mini-sclad.firebaseio.com",
    storageBucket: "mini-sclad.appspot.com",
  };

firebase.initializeApp(configFirebase);

const database =  firebase.database().ref();
const user = firebase.auth();

export {database, user};