import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import React, { Component } from 'react'
import widthLoading from '../../hoc/widthLoading';





 class AuthForm extends Component {
    state = {
        email:'',
        password: ''
      };

    handleChange = node => this.setState({[node.currentTarget.type] : node.currentTarget.value})

    handleClick = () => {
      const {email,password} = this.state;
     
      if(email && password){
          this.props.functions.auth(email,password); 
      }
    }

    render() {
      const {signIn} = this.props;
      const {email,password} = this.state;
      return (
          <form  noValidate autoComplete="off">
          <TextField
            id="outlined-email-input"
            label="Email"
            type="email"
            name="email"
            autoComplete="email"
            margin="normal"
            variant="outlined"
            onChange = {this.handleChange}
          />
          <TextField
            id="outlined-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            margin="normal"
            variant="outlined"
            onChange = {this.handleChange}
          />
          <Button variant="contained" color="primary" onClick = {signIn.bind(this,email,password)}>
              Авторизация
          </Button>
          
        
        </form>
      )
  }
}

export default AuthForm;